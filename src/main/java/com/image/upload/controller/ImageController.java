package com.image.upload.controller;

import com.image.upload.config.response.BaseResponse;
import com.image.upload.dto.Image.ImageDto;
import com.image.upload.model.ImageEntity;
import com.image.upload.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("api/v1/upload-images")
public class ImageController {

    @Autowired
    private ImageService imageService;

    @GetMapping
    public ResponseEntity<List<ImageEntity>> listImage(){
        return imageService.listImageEntity();
    }

    @GetMapping("{id}")
    public ResponseEntity<ImageEntity> getImageById(@PathVariable Long id) {
        return imageService.getImageById(id);
    }

    @PostMapping
    public ResponseEntity<ImageEntity> addImage(@RequestParam("file")MultipartFile file, ImageEntity newImage) {
        return imageService.addImage(file, newImage);
    }

    //PUT DATA
    @PutMapping(value = "{id}", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ImageEntity> editImage(@RequestBody ImageEntity editImage, @PathVariable Long id) {
        return imageService.editImage(editImage, id);
    }

    //DETELE DATA
    @DeleteMapping("{id}")
    public ResponseEntity<ImageEntity> deleteImage(@PathVariable Long id) {
        return imageService.deleteImage(id);
    }

}
